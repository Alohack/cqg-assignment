#include <iostream>
#include <string>
#include <map>
#include <queue>

struct Request
{
    std::string trader_id;
    bool is_buying;
    std::size_t qty;
    std::size_t price;

    char side_sign()
    {
        return is_buying? '+' : '-';
    }
};
std::istream& operator>>(std::istream& in, Request& request)
{
    char side{};
    in >> request.trader_id >> side >> request.qty >> request.price;
    request.is_buying = (side == 'B');
    return in;
}





int main()
{
    std::map<std::size_t, std::queue<Request>> price_orders;
    Request request{};
    while(std::cin >> request)
    {
        auto it = price_orders.find(request.price);
        if(it == price_orders.end())
            price_orders[request.price] = std::queue<Request>();
        if(price_orders[request.price].empty() || price_orders[request.price].front().is_buying == request.is_buying)
            price_orders[request.price].push(request);
        else 
        {
            while(price_orders[request.price].empty() && request.qty > 0)
            {
                if(request.qty >= price_orders[request.price].front().qty)
                {
                    std::cout << request.trader_id << request.side_sign() << price_orders[request.price].front().qty << "@" << request.price << "\n";
                    request.qty -= price_orders[request.price].front().qty;
                    price_orders[request.price].pop();
                }
                else
                {
                    std::cout << request.trader_id << request.side_sign() << request.qty << "@" << request.price << "\n";
                    price_orders[request.price].front().qty -= request.qty;
                    request.qty = 0;
                }
            }
            if(request.qty > 0)
                price_orders[request.price].push(request);
        }
    }

    
    return 0;
}
